#define _CRT_SECURE_NO_WARNINGS
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
int main()
{
	int m[200000];
	srand(time(NULL));
	clock_t start, end;
	for (int nn = 0; nn < 10; nn++) {
		int n = nn == 0 ? 10 : pow(2, nn + 8);

		for (int i = 0; i < n; i++) {
			m[i] = rand();
			printf(nn == 0 ? "%d " : "", m[i]);
		}
		int min;
		start = clock();
		for (int i = 1; i < n; i++)
		{
			int t = m[i];
			for (int j = i-1; j >=0 ; j--)
			{
				if (m[j] < t)
					break;
				m[j+1] = m[j];
				m[j] = t;
			}
		}
		end = clock();
		if (nn == 0) {
			printf("\n");
			for (int i = 0; i < n; i++) {
				printf("%d ", m[i]);
			}
		}
		float time = (end - start) / (float)CLOCKS_PER_SEC;
		printf("\n%d,%.5f", n, time);
	}
	return 0;
}
