#define _CRT_SECURE_NO_WARNINGS
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
int main()
{
	int m[200000];
	srand(time(NULL));
	clock_t start, end;
	for (int nn = 0; nn < 10; nn++) {
		int n = nn == 0 ? 10 : pow(2, nn + 8);
		for (int i = 0; i < n; i++) {
			m[i] = rand();
			printf(nn == 0 ? "%d " : "", m[i]);
		}
		start = clock();

		for (int s = n; s > 0; s /= 2) {
			for (int i = 0; i < n - s; i++) {
				int j = i;
				while (j >= 0 && m[j] > m[j + s])
				{
					int c = m[j];
					m[j] = m[j + s];
					m[j + s] = c;
					j--;
				}
			}
		}
		end = clock();
		if (nn == 0) {
			printf("\n");
			for (int i = 0; i < n; i++) {
				printf("%d ", m[i]);
			}
		}
		float time = (end - start) / (float)CLOCKS_PER_SEC;
		printf("\n%d,%.5f", n, time);
	}
	return 0;
}
